//! A collection of pattern matching algorithms.

use katexit::katexit;

pub mod kmp;
pub mod rabin_karp;

#[katexit]
/// does pattern matching the naive way.
///
/// complexity: $\\mathcal{O}(|\\text{text}| * |\\text{pattern}|)$
pub fn naive_pattern_matching(text: &[u8], pattern: &[u8]) -> Option<usize> {
    let m = pattern.len();
    for (idx, window) in text.windows(m).enumerate() {
        if window == pattern {
            return Some(idx);
        }
    }
    None
}
